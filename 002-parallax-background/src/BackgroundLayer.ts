export class BackgroundLayer {
  x: number;
  y: number;
  speed: number;
  constructor(
    public image: HTMLImageElement,
    public modifier: number,
    public width: number,
    public height: number,
    public gameSpeed: number,
  ) {
    this.speed = gameSpeed * modifier;
    this.x = 0;
    this.y = 0;
  }

  public updateSpeed(gameSpeed: number) {
    this.speed = gameSpeed * this.modifier;
  }

  public animate(ctx: CanvasRenderingContext2D) {
    this.x = this.x   <= -this.width ? 0 : this.x  - this.speed;

    ctx.drawImage(this.image, this.x, 0);
    ctx.drawImage(this.image, this.x + this.width, 0);
  }
}
