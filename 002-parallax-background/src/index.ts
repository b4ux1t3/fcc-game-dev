import { BackgroundLayer } from './BackgroundLayer';

const canvas = document.getElementById('main-canvas') as HTMLCanvasElement;
const ctx = canvas.getContext('2d');

const slider = document.getElementById('slider') as HTMLInputElement;
const sliderLabel = document.getElementById('slider-label') as HTMLLabelElement;

const CANVAS_WIDTH = canvas.width = 800;
const CANVAS_HEIGHT = canvas.height = 700;

let gameSpeed = 5;

const imageLayers: Array<BackgroundLayer> = [];

for (let i = 1; i <= 5; i++){
  const backgroundLayer = new Image();
  backgroundLayer.src = `img/layer-${i}.png`;
  imageLayers.push(new BackgroundLayer(backgroundLayer, 1 - (1/i), 2400, 700, gameSpeed));
}


function configureSlider(slider: HTMLInputElement) {
  slider.min = '0';
  slider.max = '15';
  slider.step = '1';
  slider.value = '5';
  slider.addEventListener('change', () => {
    gameSpeed = parseInt(slider.value);
    sliderLabel.innerText = slider.value;
    imageLayers.forEach(layer => layer.updateSpeed(gameSpeed));
  });
  sliderLabel.innerText = slider.value;
}

configureSlider(slider);

function clear() {
  ctx.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT)
}

function drawBackground(){
  imageLayers.forEach(image => {
    image.animate(ctx);
  })
}

function animate() {
  clear();
  drawBackground();
  requestAnimationFrame(animate);
}

animate();
