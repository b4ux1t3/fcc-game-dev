# FreeCodeCamp Game Development in ~~Java~~TypeScript

FreeCodeCamp recently released a course by the esteemed [Frank Dvorak](https://www.youtube.com/c/Frankslaboratory). I've decided to do the course, but in TypeScript. Feel free to see the implementations in action [here](https://b4ux1t3.gitlab.io/fcc-game-dev)!

There are no libraries, third-party scripts, or any of that nonsense. Not even Bootstrap!
