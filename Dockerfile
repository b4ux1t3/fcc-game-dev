FROM node:latest as build

WORKDIR /build

COPY 001-sprite-animation /build/001

WORKDIR /build/001

RUN npm ci
RUN npm run build

WORKDIR /build

COPY 002-parallax-background /build/002

WORKDIR /build/002

RUN npm ci
RUN npm run build

COPY 003-enemy-movement /build/003

WORKDIR /build/003

RUN npm ci
RUN npm run build



FROM nginx:latest
WORKDIR /app

COPY index.html /usr/share/nginx/html
COPY style /usr/share/nginx/html/style
COPY --from=build /build/001/dist /usr/share/nginx/html/001
COPY --from=build /build/002/dist /usr/share/nginx/html/002
COPY --from=build /build/003/dist /usr/share/nginx/html/003
