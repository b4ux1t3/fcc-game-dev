export class SpriteSheet{

  public frames = 0;
  constructor(public spriteWidth: number, public spriteHeight: number, public sheet: HTMLImageElement) {
    this.frames = sheet.naturalWidth / spriteWidth;
  }

  setFrames(){
    this.frames = this.sheet.naturalWidth / this.spriteWidth;
  }
}
