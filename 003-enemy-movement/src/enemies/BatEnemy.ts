import { SpriteSheet } from "../Animations";
import { CANVAS_HEIGHT, CANVAS_WIDTH } from "../environment";
import { Vector2 } from "../Vector2";
import { Enemy } from "./Enemy";

export class BatEnemy implements Enemy {
  public position: Vector2

  private animationFrameOffset;
  private maxVelocity = 2;
  private animationSpeed = 5;
  private velocity: Vector2;
  private spriteSheet: SpriteSheet;
  private width: number;
  private height: number;

  constructor(x: number, y: number, public scale: number, private ctx: CanvasRenderingContext2D) {
    this.position = new Vector2(x, y);
    this.velocity = new Vector2(Math.random() * 4 - 2, Math.random() * 4 - 2);
    this.velocity.normalize();
    const image = new Image();
    image.src = `img/enemy1.png`;
    this.spriteSheet = new SpriteSheet(293, 155, image);
    image.addEventListener('load', () => this.spriteSheet.setFrames());
    this.animationFrameOffset = Math.floor(Math.random() * this.spriteSheet.frames);
    this.animationSpeed =  Math.floor(Math.random() * 10) + 1;

    this.width = this.spriteSheet.spriteWidth * this.scale;
    this.height = this.spriteSheet.spriteHeight * this.scale;
  }

  info(): HTMLDivElement {
    const firstP = document.createElement('p');
    firstP.innerText = `Velocity:${this.velocity.x.toFixed(2)}, ${this.velocity.y.toFixed(2)}`;
    const newDiv = document.createElement('div');
    newDiv.appendChild(firstP);

    return newDiv;
  }

  inBoundingBox(x: number, y: number): boolean {
    return x >= this.position.x - this.width / 2 && x < this.position.x + this.width / 2 && y >= this.position.y - this.height / 2 && y < this.position.y + this.height / 2;
  }

  update(pause: boolean){
    if (pause) return;
    this.position = this.position.add(this.velocity);
    if (this.position.x <= 0 || this.position.x >= CANVAS_WIDTH || this.position.y <= 0 || this.position.y >= CANVAS_HEIGHT)
      this.velocity.scale(-2);
    this.velocity = this.velocity.add({x: Math.random() - 0.5, y: Math.random() - 0.5} as Vector2);
    this.velocity.normalize();
    this.velocity.scale(this.maxVelocity);
  }

  draw(gameFrame: number, hitBox: boolean) {
    if (hitBox){
      this.ctx.fillStyle = 'red';
      this.ctx.fillRect(
        this.position.x - (this.width / 2),
        this.position.y - (this.height / 2),
        this.spriteSheet.spriteWidth * this.scale,
        this.spriteSheet.spriteHeight * this.scale);
    }
    this.ctx.drawImage(
      this.spriteSheet.sheet,
      ((Math.floor(gameFrame/this.animationSpeed) + this.animationFrameOffset) % this.spriteSheet.frames) * this.spriteSheet.spriteWidth,
      0,
      this.spriteSheet.spriteWidth,
      this.spriteSheet.spriteHeight,
      this.position.x - (this.width / 2),
      this.position.y - (this.height / 2),
      this.spriteSheet.spriteWidth * this.scale,
      this.spriteSheet.spriteHeight * this.scale
    );
  }
}
