import { SpriteSheet } from "../Animations";
import { CANVAS_HEIGHT, CANVAS_WIDTH } from "../environment";
import { Vector2 } from "../Vector2";
import { Enemy } from "./Enemy";

export class GhostEnemy implements Enemy{
  position: Vector2;
    private velocity: Vector2;
    private spriteSheet: SpriteSheet;
  private animationFrameOffset: number;
  private animationSpeed: number;

  private width: number;
  private height: number;


  private angle: number;
  private angleSpeed: number;
  private amplitude: number;
  private xPeriodRatio: number;
  private yPeriodRatio: number;

  constructor(x: number, y: number, private scale: number, private ctx: CanvasRenderingContext2D){
    this.position = new Vector2(x, y);
    this.velocity = new Vector2(0, 0);
    const image = new Image();
    image.src = `img/enemy3.png`;
    this.spriteSheet = new SpriteSheet(218, 177, image);
    image.addEventListener('load', () => this.spriteSheet.setFrames());
    this.animationFrameOffset = Math.floor(Math.random() * this.spriteSheet.frames);
    this.animationSpeed =  Math.floor(Math.random() * 10) + 5;

    this.width = this.spriteSheet.spriteWidth * this.scale;
    this.height = this.spriteSheet.spriteHeight * this.scale;
    this.randomizeWave();
  }

  info(): HTMLDivElement {
    const secondP = document.createElement('p');
    secondP.innerText = `Amplitude: ${this.amplitude.toFixed(2)}`;
    const thirdP = document.createElement('p');
    thirdP.innerText = `X Axis Offset: ${this.xPeriodRatio.toFixed(2)}`;
    const fourthP = document.createElement('p');
    fourthP.innerText = `Y Axis Offset: ${this.yPeriodRatio.toFixed(2)}`;
    const newDiv = document.createElement('div');
    newDiv.appendChild(secondP);
    newDiv.appendChild(thirdP);
    newDiv.appendChild(fourthP);

    return newDiv;
  }

  inBoundingBox(x: number, y: number): boolean {
    return x >= this.position.x - this.width / 2 && x < this.position.x + this.width / 2 && y >= this.position.y - this.height / 2 && y < this.position.y + this.height / 2;
  }

  update(pause: boolean): void {
    if (pause) return;
    this.position = new Vector2(
      Math.cos(this.angle * Math.PI/this.xPeriodRatio) * (CANVAS_WIDTH / 2) + (CANVAS_WIDTH / 2),
      Math.sin(this.angle * Math.PI/this.yPeriodRatio) * (CANVAS_HEIGHT / 2) + (CANVAS_HEIGHT / 2)
    );
    this.angle += this.angleSpeed;
  }

  draw(gameFrame: number, hitBox: boolean) {
    if (hitBox){
      this.ctx.fillStyle = 'red';
      this.ctx.fillRect(
        this.position.x - (this.width / 2),
        this.position.y - (this.height / 2),
        this.spriteSheet.spriteWidth * this.scale,
        this.spriteSheet.spriteHeight * this.scale);
    }
    this.ctx.drawImage(
      this.spriteSheet.sheet,
      ((Math.floor(gameFrame/this.animationSpeed) + this.animationFrameOffset) % this.spriteSheet.frames) * this.spriteSheet.spriteWidth,
      0,
      this.spriteSheet.spriteWidth,
      this.spriteSheet.spriteHeight,
      this.position.x - (this.width / 2),
      this.position.y - (this.height / 2),
      this.spriteSheet.spriteWidth * this.scale,
      this.spriteSheet.spriteHeight * this.scale
    );
  }


  private randomizeWave(){
    this.angle = Math.random() * 2 * Math.PI;
    this.angleSpeed = Math.random() * 1.5 + 0.5;
    this.amplitude = Math.random() * 3;
    this.xPeriodRatio = Math.random() * 180 + 180;
    this.yPeriodRatio = Math.random() * 180 + 180;
  }
}
