import { SpriteSheet } from "../Animations";
import { CANVAS_HEIGHT, CANVAS_WIDTH } from "../environment";
import { Vector2 } from "../Vector2";
import { Enemy } from "./Enemy";

export class SawEnemy implements Enemy{
  position: Vector2;
  private spriteSheet: SpriteSheet;
  private animationFrameOffset: number;
  private animationSpeed: number;


  private width: number;
  private height: number;

  private velocity: Vector2
  private destination: Vector2;

  private speed: number;


  constructor(x: number, y: number, private scale: number, private ctx: CanvasRenderingContext2D){
    this.position = new Vector2(x, y);
    const image = new Image();
    image.src = `img/enemy4.png`;
    this.spriteSheet = new SpriteSheet(213, 212, image);
    image.addEventListener('load', () => this.spriteSheet.setFrames());
    this.animationFrameOffset = Math.floor(Math.random() * this.spriteSheet.frames);
    this.animationSpeed = 3;
    this.width = this.spriteSheet.spriteWidth * this.scale;
    this.height = this.spriteSheet.spriteHeight * this.scale;

    this.speed = Math.random() * 4 + 1;

    this.newDestination();
  }

  info(): HTMLDivElement {
    return document.createElement('div');
  }

  inBoundingBox(x: number, y: number): boolean {
    return x >= this.position.x - this.width / 2 && x < this.position.x + this.width / 2 && y >= this.position.y - this.height / 2 && y < this.position.y + this.height / 2;
  }

  update(pause: boolean): void {
    if (pause) return;
    this.position = this.position.add(this.velocity);
    if (this.position.distance(this.destination) <= this.speed) this.newDestination();
  }

  draw(gameFrame: number, hitBox: boolean) {
    if (hitBox){
      this.ctx.beginPath();
      this.ctx.ellipse(
        this.position.x,
        this.position.y,
        this.spriteSheet.spriteWidth * this.scale / 2,
        this.spriteSheet.spriteHeight * this.scale / 2,
        0, 0, 360);
      this.ctx.fillStyle ='red';
      this.ctx.fill();
    }
    this.ctx.drawImage(
      this.spriteSheet.sheet,
      ((Math.floor(gameFrame/this.animationSpeed) + this.animationFrameOffset) % this.spriteSheet.frames) * this.spriteSheet.spriteWidth,
      0,
      this.spriteSheet.spriteWidth,
      this.spriteSheet.spriteHeight,
      this.position.x - (this.width / 2),
      this.position.y - (this.height / 2),
      this.spriteSheet.spriteWidth * this.scale,
      this.spriteSheet.spriteHeight * this.scale
    );
    if (hitBox){
      this.ctx.moveTo(this.position.x, this.position.y);
      this.ctx.lineTo(this.destination.x, this.destination.y);
      this.ctx.stroke();
    }
  }

  private calculateVelocity(){
    this.velocity = this.destination.add(this.position.multiply(-1));
    this.velocity.normalize();
    this.velocity = this.velocity.multiply(this.speed);
  }

  private newDestination(){
    this.destination = new Vector2(
      Math.floor(Math.random() * CANVAS_WIDTH),
      Math.floor(Math.random() * CANVAS_HEIGHT)
    );
    this.calculateVelocity();
  }
}
