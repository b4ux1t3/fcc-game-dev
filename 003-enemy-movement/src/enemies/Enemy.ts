import { Vector2 } from "../Vector2";
import { BatEnemy } from "./BatEnemy";
import { CreepyBatEnemy } from "./CreepyBatEnemy";
import { GhostEnemy } from "./GhostEnemy";
import { SawEnemy } from "./SawEnemy";
export { BatEnemy };
export { CreepyBatEnemy };
export { SawEnemy };
export { GhostEnemy };

export interface Enemy {
  position: Vector2;
  update(pause: boolean): void;
  draw(gameFrame: number, hitBox: boolean): void;
  inBoundingBox(x: number, y: number): boolean;
  info(): HTMLDivElement;
}

export type EnemyType = 'bat' | 'creepy bat' | 'ghost' | 'saw';
export function getEnemyType(enemyType: string) : EnemyType {
  switch (enemyType){
    case 'bat':
    case 'creepy bat':
    case 'ghost':
    case 'saw':
      return enemyType;
    default:
     return 'bat';
  }
}

export function generateEnemy(enemyType: EnemyType, x: number, y: number, scale: number, ctx: CanvasRenderingContext2D): Enemy {
  switch (enemyType){
    case 'bat':
     return new BatEnemy(x, y, scale, ctx);
    case 'creepy bat':
      return new CreepyBatEnemy(x, y, scale, ctx);
    case 'ghost':
      return new GhostEnemy(x, y, scale, ctx);
    case 'saw':
      return new SawEnemy(x, y, scale, ctx);
    }
}
