import { SpriteSheet } from "../Animations";
import { CANVAS_WIDTH } from "../environment";
import { Vector2 } from "../Vector2";
import { Enemy } from "./Enemy";

export class CreepyBatEnemy implements Enemy{
  position: Vector2;
  private velocity: Vector2;
  private spriteSheet: SpriteSheet;
  private animationFrameOffset: number;
  private animationSpeed: number;
  private width: number;
  private height: number;
  private angle: number;
  private angleSpeed: number;
  private amplitude: number;
  private period: number;


  constructor(x: number, y: number, private scale: number, private ctx: CanvasRenderingContext2D){
    this.position = new Vector2(x, y);
    this.velocity = new Vector2(-1, 0);
    const image = new Image();
    image.src = `img/enemy2.png`;
    this.spriteSheet = new SpriteSheet(266, 188, image);
    image.addEventListener('load', () => this.spriteSheet.setFrames());
    this.animationFrameOffset = Math.floor(Math.random() * this.spriteSheet.frames);
    this.animationSpeed =  Math.floor(Math.random() * 5) + 5;

    this.width = this.spriteSheet.spriteWidth * this.scale;
    this.height = this.spriteSheet.spriteHeight * this.scale;
    this.randomizeWave();
  }
  info(): HTMLDivElement {
    const firstP = document.createElement('p');
    firstP.innerText = `Velocity:${this.velocity.x.toFixed(2)}, ${this.velocity.y.toFixed(2)}`;
    const secondP = document.createElement('p');
    secondP.innerText = `Amplitude: ${this.amplitude.toFixed(2)}`;
    const thirdP = document.createElement('p');
    thirdP.innerText = `Period: ${this.period.toFixed(2)}`;
    const newDiv = document.createElement('div');
    newDiv.appendChild(firstP);
    newDiv.appendChild(secondP);
    newDiv.appendChild(thirdP);

    return newDiv;
  }

  inBoundingBox(x: number, y: number): boolean {
    return x >= this.position.x - this.width / 2 && x < this.position.x + this.width / 2 && y >= this.position.y - this.height / 2 && y < this.position.y + this.height / 2;
  }

  update(pause: boolean): void {
    if (pause) return;
    this.position = this.position.add(this.velocity);
    if (this.position.x < -this.width) {
      this.position.x = CANVAS_WIDTH + this.width;
      this.randomizeWave();
    }
    this.position.y += Math.sin(this.angle * this.period) * this.amplitude;
    this.angle += this.angleSpeed;
  }

  draw(gameFrame: number, hitBox: boolean) {
    if (hitBox){
      this.ctx.fillStyle = 'red';
      this.ctx.fillRect(
        this.position.x - (this.width / 2),
        this.position.y - (this.height / 2),
        this.spriteSheet.spriteWidth * this.scale,
        this.spriteSheet.spriteHeight * this.scale);
    }
    this.ctx.drawImage(
      this.spriteSheet.sheet,
      ((Math.floor(gameFrame/this.animationSpeed) + this.animationFrameOffset) % this.spriteSheet.frames) * this.spriteSheet.spriteWidth,
      0,
      this.spriteSheet.spriteWidth,
      this.spriteSheet.spriteHeight,
      this.position.x - (this.width / 2),
      this.position.y - (this.height / 2),
      this.spriteSheet.spriteWidth * this.scale,
      this.spriteSheet.spriteHeight * this.scale
    );
  }

  private randomizeWave(){
    this.angle = Math.random() * 2 * Math.PI;
    this.angleSpeed = Math.random() * 0.2;
    this.amplitude = Math.random() * 6;
    this.period = Math.random();
  }

}
