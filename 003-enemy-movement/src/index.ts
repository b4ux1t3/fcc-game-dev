import { Enemy, EnemyType, getEnemyType, generateEnemy } from "./enemies/Enemy";
import { CANVAS_HEIGHT, CANVAS_WIDTH } from "./environment";

const canvas = document.getElementById('main-canvas') as HTMLCanvasElement;
const ctx = canvas.getContext('2d');

const enemyTypes = ['bat', 'creepy bat', 'ghost', 'saw'];

const dropdown = document.getElementById('enemy-type') as HTMLSelectElement;
const debugCheckbox = document.getElementById('bounding-boxes') as HTMLInputElement;
const enemyDetails = document.getElementById('enemy-details') as HTMLDivElement;
const numEnemiesInput = document.getElementById('num-enemies') as HTMLInputElement;

canvas.width = CANVAS_WIDTH;
canvas.height = CANVAS_HEIGHT;

let gameFrames = 0;
let boundingBoxes = false;

let currentEnemyType: EnemyType = 'bat';

let numEnemies = 10;
let enemies: Array<Enemy> = [];
let selectedEnemy: Enemy;

let pause = false;

function generateEnemies(enemyType: EnemyType, enemyNumber: number): Array<Enemy> {
  const enemies: Array<Enemy> = [];
  for (let i = 1; i <= enemyNumber; i++){
    const x = Math.random() * CANVAS_WIDTH;
    const y = Math.random() * CANVAS_HEIGHT;
    enemies.push (generateEnemy(enemyType, x, y, 0.3, ctx));
  }

  return enemies;
}

function configureUi(){
  enemyTypes.forEach(enemyType => {
    const newOption = document.createElement('option');
    newOption.value = enemyType;
    newOption.text = enemyType;
    dropdown.appendChild(newOption);
  });

  dropdown.addEventListener('change', () => {
    currentEnemyType = getEnemyType(dropdown.value);
    enemies = generateEnemies(currentEnemyType, numEnemies);
  });

  debugCheckbox.addEventListener('change', () => boundingBoxes = debugCheckbox.checked);

  canvas.addEventListener('mousedown', (e) => {
    const filteredEnemies = enemies.filter(enemy => enemy.inBoundingBox(e.offsetX, e.offsetY));
    if (filteredEnemies.length > 0) {
      filteredEnemies.forEach(console.log);
    }
    else {
      enemies.push(generateEnemy(currentEnemyType, e.offsetX, e.offsetY, 0.3, ctx));
      numEnemies++;
      numEnemiesInput.value = `${numEnemies}`;
    }
    selectedEnemy = filteredEnemies[filteredEnemies.length - 1];
  });

  document.addEventListener('keypress', (e) =>{
    if (e.key == ' ') pause = !pause;
    if (e.key == '`') selectedEnemy = null;
  });
  numEnemiesInput.value = `${numEnemies}`;
  numEnemiesInput.addEventListener('change', () => {
    const temp = parseInt(numEnemiesInput.value);
    if (temp < 0) return;
    const newEnemies = temp - numEnemies;
    newEnemies > 0
      ? enemies = [...enemies, ...generateEnemies(getEnemyType(dropdown.value), newEnemies)]
      : enemies.splice(numEnemies + newEnemies, -newEnemies);
    numEnemies += newEnemies;
  });
  numEnemiesInput.min = `${0}`;
}

function initializeApplication(){
  enemies = generateEnemies(getEnemyType(dropdown.value), numEnemies);
  configureUi();
}

function getEnemyDetails(enemy: Enemy): HTMLDivElement{
  const newP = document.createElement('p');
  newP.innerHTML = `Position: ${enemy.position.x.toFixed(2)}, ${enemy.position.y.toFixed(2)}`;
  const newDiv = document.createElement('div');
  newDiv.appendChild(newP);
  newDiv.appendChild(enemy.info());
  return newDiv;
}

function updateEnemyDetails() {
  enemyDetails.style.display = 'none';
  enemyDetails.innerHTML = '';
  if (!selectedEnemy) return;
  enemyDetails.style.display = '';
  enemyDetails.appendChild(getEnemyDetails(selectedEnemy));
}

function animate() {
  ctx.clearRect(0,0,CANVAS_WIDTH,CANVAS_HEIGHT);

  enemies.forEach(enemy => {
    enemy.update(pause);
    enemy.draw(gameFrames, boundingBoxes || enemy === selectedEnemy);
  })
  if (!pause) gameFrames++;

  updateEnemyDetails();
  requestAnimationFrame(animate)
}

initializeApplication();
animate();
