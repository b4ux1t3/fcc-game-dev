export const CANVAS_WIDTH = visualViewport.width < 500 ? visualViewport.width - 10 : 500;
export const CANVAS_HEIGHT = visualViewport.height < 1000 ? visualViewport.height : 1000;
