const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: './src/index.ts',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        {from: "public/*", to: "[name][ext]"},
        {from: "public/style/*", to: "style/[name][ext]"},
        {from: "public/img/*", to: "img/[name][ext]"},
      ]
    })
  ],
  devServer : {
    allowedHosts: ["dorothy", "192.168.1.151", "localhost"]
  }
};
