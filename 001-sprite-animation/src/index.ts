import { Coordinate, CustomAnimation, customAnimations } from "./Animation";
import { DEBUG } from "./environment";

const canvas: HTMLCanvasElement = document.getElementById("main-canvas") as HTMLCanvasElement;
const animationDropdown: HTMLSelectElement = document.getElementById("animations") as HTMLSelectElement;

const ctx = canvas.getContext('2d');
const CANVAS_WIDTH = canvas.width = 600;
const CANVAS_HEIGHT = canvas.height = 600;

const playerImage = new Image();
playerImage.src = 'img/shadow_dog.png';
const frameResolution = 5;

let animation = getAnimationByName("idle");
let gameFrame = 0;
let lastFrameTime = 0;
let lastSixty: Array<number> = [];

function populateOptions(dropdown: HTMLSelectElement, animations: Array<CustomAnimation>) {
  animations.forEach(anim => {
    const newChild = document.createElement("option");
    newChild.value = anim.name;
    newChild.text = anim.name;
    dropdown.appendChild(newChild);
  });

  dropdown.addEventListener("change", () => animation = getAnimationByName(dropdown.value));
}

function checkFrameRate(deltaTime: number) {
  if (deltaTime === 0) return;
  const framesPerSecond = 1 / deltaTime * 1000;

  if (lastSixty.length === 60) {
    lastSixty = [...lastSixty.slice(1), framesPerSecond]
  } else {
    lastSixty.push(framesPerSecond);
  }

  console.log(`fps: ${(lastSixty.reduce((x, y) => x + y) / lastSixty.length).toFixed(2)}`);

}

function getAnimationByName(name: string): CustomAnimation | null {
  const animations = customAnimations.filter(anim => anim.name === name);
  if (animations.length !== 1) return null;
  return animations[0];
}
function drawFrame(animation: CustomAnimation, frame: Coordinate) {
  ctx.drawImage(playerImage, frame.x, frame.y, animation.spriteWidth, animation.spriteHeight, 0, 0, animation.spriteWidth / 10, animation.spriteHeight / 10);
}

function animate(timeStamp: number) {
  const newFrame = timeStamp != lastFrameTime;
  const deltaTime = timeStamp - lastFrameTime;
  lastFrameTime = timeStamp;

  if (DEBUG) checkFrameRate(deltaTime);

  if (newFrame){
    ctx.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);

    const frame = animation.frames[Math.floor(gameFrame++/frameResolution) % animation.frames.length];

    drawFrame(animation, frame);
  }
  requestAnimationFrame(animate);
}
populateOptions(animationDropdown, customAnimations);
animate(0);
