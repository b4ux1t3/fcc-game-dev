export type AnimationRow = {
  name: string;
  yIndex: number;
  frames: number;
}

export type CustomAnimation = {
  name: string;
  spriteWidth: number;
  spriteHeight: number;
  frames: Array<Coordinate>;
}

export type Coordinate = {
  x: number;
  y: number;
}

export const animationRows = [
  {
    name: "idle",
    yIndex: 0,
    frames: 7,
  } as AnimationRow,
  {
    name: "jump",
    yIndex: 1,
    frames: 7,
  } as AnimationRow,
  {
    name: "fall",
    yIndex: 2,
    frames: 7,
  } as AnimationRow,
  {
    name: "run",
    yIndex: 3,
    frames: 9,
  } as AnimationRow,
  {
    name: "daze",
    yIndex: 4,
    frames: 11,
  } as AnimationRow,
  {
    name: "lie-down",
    yIndex: 5,
    frames: 5,
  } as AnimationRow,
  {
    name: "roll",
    yIndex: 6,
    frames: 7,
  } as AnimationRow,
  {
    name: "bite",
    yIndex: 7,
    frames: 7,
  } as AnimationRow,
  {
    name: "die",
    yIndex: 8,
    frames: 12,
  } as AnimationRow,
  {
    name: "hit",
    yIndex: 9,
    frames: 4,
  } as AnimationRow,
]
export const customAnimations = [
  {
    name: "idle",
    spriteHeight: 523,
    spriteWidth: 575,
    frames: [
      {
        x: 0,
        y: 0
      } as Coordinate,
      {
        x: 1 * 575,
        y: 0
      } as Coordinate,
      {
        x: 2 * 575,
        y: 0
      } as Coordinate,
      {
        x: 3 * 575,
        y: 0
      } as Coordinate,
      {
        x: 4 * 575,
        y: 0
      } as Coordinate,
      {
        x: 5 * 575,
        y: 0
      } as Coordinate,
      {
        x: 6 * 575,
        y: 0
      } as Coordinate,
    ]
  } as CustomAnimation,
  {
    name: "jump",
    spriteHeight: 523,
    spriteWidth: 575,
    frames: [
      {
        x: 0,
        y: 1 * 523
      } as Coordinate,
      {
        x: 1 * 575,
        y: 1 * 523
      } as Coordinate,
      {
        x: 2 * 575,
        y: 1 * 523
      } as Coordinate,
      {
        x: 3 * 575,
        y: 1 * 523
      } as Coordinate,
      {
        x: 4 * 575,
        y: 1 * 523
      } as Coordinate,
      {
        x: 5 * 575,
        y: 1 * 523
      } as Coordinate,
      {
        x: 6 * 575,
        y: 1 * 523
      } as Coordinate,
    ]
  } as CustomAnimation,
  {
    name: "fall",
    spriteHeight: 523,
    spriteWidth: 575,
    frames: [
      {
        x: 0,
        y: 2 * 523
      } as Coordinate,
      {
        x: 1 * 575,
        y: 2 * 523
      } as Coordinate,
      {
        x: 2 * 575,
        y: 2 * 523
      } as Coordinate,
      {
        x: 3 * 575,
        y: 2 * 523
      } as Coordinate,
      {
        x: 4 * 575,
        y: 2 * 523
      } as Coordinate,
      {
        x: 5 * 575,
        y: 2 * 523
      } as Coordinate,
      {
        x: 6 * 575,
        y: 2 * 523
      } as Coordinate,
    ]
  } as CustomAnimation,
  {
    name: "run",
    spriteHeight: 523,
    spriteWidth: 575,
    frames: [
      {
        x: 0,
        y: 3 * 523
      } as Coordinate,
      {
        x: 1 * 575,
        y: 3 * 523
      } as Coordinate,
      {
        x: 2 * 575,
        y: 3 * 523
      } as Coordinate,
      {
        x: 3 * 575,
        y: 3 * 523
      } as Coordinate,
      {
        x: 4 * 575,
        y: 3 * 523
      } as Coordinate,
      {
        x: 5 * 575,
        y: 3 * 523
      } as Coordinate,
      {
        x: 6 * 575,
        y: 3 * 523
      } as Coordinate,
      {
        x: 7 * 575,
        y: 3 * 523
      } as Coordinate,
      {
        x: 8 * 575,
        y: 3 * 523
      } as Coordinate,
    ]
  } as CustomAnimation,
  {
    name: "daze",
    spriteHeight: 523,
    spriteWidth: 575,
    frames: [
      {
        x: 0,
        y: 4 * 523
      } as Coordinate,
      {
        x: 1 * 575,
        y: 4 * 523
      } as Coordinate,
      {
        x: 2 * 575,
        y: 4 * 523
      } as Coordinate,
      {
        x: 3 * 575,
        y: 4 * 523
      } as Coordinate,
      {
        x: 4 * 575,
        y: 4 * 523
      } as Coordinate,
      {
        x: 5 * 575,
        y: 4 * 523
      } as Coordinate,
      {
        x: 6 * 575,
        y: 4 * 523
      } as Coordinate,
      {
        x: 7 * 575,
        y: 4 * 523
      } as Coordinate,
      {
        x: 8 * 575,
        y: 4 * 523
      } as Coordinate,
      {
        x: 9 * 575,
        y: 4 * 523
      } as Coordinate,
      {
        x: 10 * 575,
        y: 4 * 523
      } as Coordinate,
    ]
  } as CustomAnimation,
  {
    name: "lie-down",
    spriteHeight: 523,
    spriteWidth: 575,
    frames: [
      {
        x: 0,
        y: 5 * 523
      } as Coordinate,
      {
        x: 1 * 575,
        y: 5 * 523
      } as Coordinate,
      {
        x: 2 * 575,
        y: 5 * 523
      } as Coordinate,
      {
        x: 3 * 575,
        y: 5 * 523
      } as Coordinate,
      {
        x: 4 * 575,
        y: 5 * 523
      } as Coordinate,
    ]
  } as CustomAnimation,
  {
    name: "roll",
    spriteHeight: 523,
    spriteWidth: 575,
    frames: [
      {
        x: 0,
        y: 6 * 523
      } as Coordinate,
      {
        x: 1 * 575,
        y: 6 * 523
      } as Coordinate,
      {
        x: 2 * 575,
        y: 6 * 523
      } as Coordinate,
      {
        x: 3 * 575,
        y: 6 * 523
      } as Coordinate,
      {
        x: 4 * 575,
        y: 6 * 523
      } as Coordinate,
      {
        x: 5 * 575,
        y: 6 * 523
      } as Coordinate,
      {
        x: 6 * 575,
        y: 6 * 523
      } as Coordinate,
    ]
  } as CustomAnimation,
  {
    name: "bite",
    spriteHeight: 523,
    spriteWidth: 575,
    frames: [
      {
        x: 0,
        y: 7 * 523
      } as Coordinate,
      {
        x: 1 * 575,
        y: 7 * 523
      } as Coordinate,
      {
        x: 2 * 575,
        y: 7 * 523
      } as Coordinate,
      {
        x: 3 * 575,
        y: 7 * 523
      } as Coordinate,
      {
        x: 4 * 575,
        y: 7 * 523
      } as Coordinate,
      {
        x: 5 * 575,
        y: 7 * 523
      } as Coordinate,
      {
        x: 6 * 575,
        y: 7 * 523
      } as Coordinate,
    ]
  } as CustomAnimation,
  {
    name: "die",
    spriteHeight: 523,
    spriteWidth: 575,
    frames: [
      {
        x: 0,
        y: 8 * 523
      } as Coordinate,
      {
        x: 1 * 575,
        y: 8 * 523
      } as Coordinate,
      {
        x: 2 * 575,
        y: 8 * 523
      } as Coordinate,
      {
        x: 3 * 575,
        y: 8 * 523
      } as Coordinate,
      {
        x: 4 * 575,
        y: 8 * 523
      } as Coordinate,
      {
        x: 5 * 575,
        y: 8 * 523
      } as Coordinate,
      {
        x: 6 * 575,
        y: 8 * 523
      } as Coordinate,
      {
        x: 7 * 575,
        y: 8 * 523
      } as Coordinate,
      {
        x: 8 * 575,
        y: 8 * 523
      } as Coordinate,
      {
        x: 9 * 575,
        y: 8 * 523
      } as Coordinate,
      {
        x: 10 * 575,
        y: 8 * 523
      } as Coordinate,
      {
        x: 11 * 575,
        y: 8 * 523
      } as Coordinate,
    ]
  } as CustomAnimation,
  {
    name: "hit",
    spriteHeight: 523,
    spriteWidth: 575,
    frames: [
      {
        x: 0,
        y: 9 * 523
      } as Coordinate,
      {
        x: 1 * 575,
        y: 9 * 523
      } as Coordinate,
      {
        x: 2 * 575,
        y: 9 * 523
      } as Coordinate,
      {
        x: 3 * 575,
        y: 9 * 523
      } as Coordinate,
    ]
  } as CustomAnimation,
]
